[Bludit](https://www.bludit.com/)
================================
**Simple**, **Fast** and **Flexible** CMS

Bludit is a web application to build your own **website** or **blog** in seconds, it's completely **free and open source**. Bludit uses files in JSON format to store the content, you don't need to install or configure a database. You only need a web server with PHP support.

Bludit is a **Flat-File** CMS.

Bludit supports **Markdown** and **HTML code** for the content.

- [Plugins](https://plugins.bludit.com)
- [Themes](https://themes.bludit.com)
- [Documentation](https://docs.bludit.com)
- Help and Support [Forum](https://forum.bludit.org) and [Chat](https://gitter.im/bludit/support)

[![Bludit PRO](https://img.shields.io/badge/Bludit-PRO-blue.svg)](https://pro.bludit.com/)
[![Backers](https://opencollective.com/bludit/tiers/backer/badge.svg?label=Backer&color=red)](https://opencollective.com/bludit)
[![Sponsors](https://opencollective.com/bludit/tiers/sponsor/badge.svg?label=Sponsor&color=brightgreen)](https://opencollective.com/bludit)

Social Networks
---------------

- [Twitter](https://twitter.com/bludit)
- [Facebook](https://www.facebook.com/bluditcms)
- [Google+](https://plus.google.com/+Bluditcms)
- [Youtube](https://www.youtube.com/channel/UCuLu0Z_CHBsTiYTDz129x9Q?view_as=subscriber)

Requirements
------------

You just need a web server with PHP support.

- PHP v5.3 or higher.
- PHP [mbstring](http://php.net/manual/en/book.mbstring.php) module for full UTF-8 support.
- PHP [gd](http://php.net/manual/en/book.image.php) module for image processing.
- PHP [dom](http://php.net/manual/en/book.dom.php) module for DOM manipulation.
- PHP [json](http://php.net/manual/en/book.json.php) module for JSON manipulation.
- Supported web servers:
   * Bludit supports almost every web server
   * PHP Built-in web server (`php -S localhost:8000`)

Installation Guide
------------------

1. Download the latest version from the official page. [Bludit.com](https://www.bludit.com)
2. Extract the zip file into a directory like `bludit`.
3. Upload the directory `bludit` to your web server or hosting.
4. Visit your domain https://example.com/bludit/
5. Follow the Bludit Installer to configure the website.

Docker Image
------------
Bludit provides an official Docker image.
- https://hub.docker.com/r/bludit/docker/

Backers
-------
Become a **Backer** and support Bludit with a monthly contribution to help us continue development.
- [Become a Backer](https://opencollective.com/bludit#backer)

Sponsors
--------
Become a **Sponsor** and support Bludit with a monthly contribution to help us continue development.
- [Become a Sponsor](https://opencollective.com/bludit#sponsor)

<a href="https://opencollective.com/clickwork" target="_blank"><img src="https://opencollective.com/bludit/sponsor/0/avatar.svg"></a>
<a href="https://opencollective.com/kreativmind" target="_blank"><img src="https://opencollective.com/proxy/images/?src=https%3A%2F%2Flogo.clearbit.com%2Fkreativmind.com&height=60"></a>
<a href="https://opencollective.com/janrippl" target="_blank"><img src="https://opencollective.com/bludit/sponsor/2/avatar.svg"></a>

License
-------
Bludit is open source software licensed under the [MIT license](https://tldrlegal.com/license/mit-license).
